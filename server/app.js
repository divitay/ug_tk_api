const cors = require('cors')
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('./cfg');


var app = express();

app.use(function (req, res, next) {
	res.setHeader('X-Powered-By', 'dimon');
	res.setHeader('X-Content-Type-Options', 'nosniff');
	res.setHeader('X-XSS-Protection','1; mode=block');
	res.setHeader('X-Frame-Options', 'DENY');
	res.setHeader("Access-Control-Allow-Origin", "*");
	res.setHeader("Access-Control-Allow-Credentials", "true");
	res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE,HEAD');
    next();
});

app.use(cors({
	exposedHeaders: ['autorization', 'Autorization'],
	origin: ['http://localhost:3000', 'http://xn--80aaqrnh1a5b5cn.xn--p1ai', 'http://southpoint.fun']
}))

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// Static
app.use(express.static(path.join(__dirname,'../', 'public')));
require('./db').init(app);

require('./routes').init(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  err.url = req.headers.host+req.url;
  next(err);
});

app.use(function(err, req, res, next) {
	if (err){
		console.log(err);
		var url = (err.url) ? err.url : null;
		var status = err.status || 500;
		var message = (err.status && (err.status == 404)) ? 'Извините но такой страницы нет :(' : 'Извините произошла ошибка ;(';
		var errs = (app.get('env') === 'development') ? JSON.stringify(err, null, ' ') : null;
		res.status(status);
		var error = {error: status,	message:message, url:url, err:errs};
		res.send({'error': error});
	}
});

module.exports = app;
