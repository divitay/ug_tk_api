'use strict';
// Requires: Mongoose
let mongoose = require('mongoose');

// Requires: Cfg
let config = require('./cfg');


// Requires: Models
var Articles = require('./model/Articles');
var Places = require('./model/Places');
var Comments = require('./model/Comments');
var Categories = require('./model/Categories');
var Rooms = require('./model/Rooms');

var state = {
	isOpen: false,
	isError: false
}

function init() {
	if (global.Promise) {
		mongoose.Promise = global.Promise;
	}	

	mongoose.connect((config.mongoose.uri), (config.mongoose.options));
	mongoose.connection.on('error', function () {
		state.isError = true;
		console.log('[mongoose]: Connection error: ' + JSON.stringify(arguments))

	});
	mongoose.connection.once('open', function () {
		state.isOpen = true;
		console.log("[mongoose]: open")

	});
}

module.exports = {
	// Mongoose
	mongoose: mongoose,
	// State
	state: state,
	// Models
	Articles: Articles,
	Rooms: Rooms,
	Places: Places,
	Comments: Comments,
	Categories: Categories,
	// init
	init: init
}