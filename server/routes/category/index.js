const express = require('express')
const router = express.Router()
const db = require('../../db')
const $ = require('../../api-v1/$');
const auth = require('../auth.mware');

router.route('/')
  .get(async (req, res) => {
    const categories = await $._find_All('Categories', {})
    res.send({ categories })
  })

router.route('/')
  .post(auth, async (req, res) => {
    const {
      name,
      description
    } = req.body

    var newCategory = new db.Categories();
    newCategory.name = name
    newCategory.description = description

    var saved = await $._saveInBase(newCategory)

    res.send({ category: saved })
  })

router.route('/:id')
  .put(auth, async (req, res) => {
    const id = req.params.id

    const {
      name,
      description
    } = req.body

    var newCategory = await $._find_One('Categories', { _id: id })
    newCategory.name = name
    newCategory.description = description

    var saved = await $._saveInBase(newCategory)

    res.send({ category: saved })
  })

router.route('/:id')
  .delete(auth, async (req, res) => {
    const id = req.params.id
    await $._delete('Categories', { _id: id })
    res.send({ status: true })
  })

module.exports = router;
