const comment = require('./comment');
const booking = require('./booking');
const place = require('./place');
const article = require('./article');
const room = require('./room');
const category = require('./category');
const uploader = require('./uploader');
const admin = require('./admin');

module.exports = {
	init: (app) => {
		// app.use('/', main);
		app.use('/comment', comment);
		app.use('/booking', booking);
		app.use('/category', category);
		app.use('/article', article);
		app.use('/place', place);
		app.use('/room', room);
		app.use('/upload', uploader);
		app.use('/admin', admin);
		// api
		// app.use( api, apiRoute);
	}
};

// 'get => /category'
// 'post => /category' { name, description }
// 'delete => /category/:id'

// 'get => /comment/:roomId'
// 'post => /comment'
// 'delete => /comment/:id'


// 'get => /article'
// 'post => /article'
// 'delete => /article/:id'


// 'get => /place'
// 'post => /place'
// 'delete => /place/:id'


// 'get => /room'
// 'post => /room'
// 'delete => /room/:id'