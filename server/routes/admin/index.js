const express = require('express')
const router = express.Router()
const auth = require('../auth.mware')
const config = require("../../cfg")

router.route('/auth')
  .post(async (req, res) => {
    const { login, password } = req.body
    if (login !== 'admin' || password !== config.authToken) {
      return res.status(403).send()
    }

    res.status(200).send({ token: config.authToken })
  })

router.route('/check', auth)
  .get(auth, async (req, res) => {
    res.status(200).send({ status: true })
  })

module.exports = router;
