const express = require('express')
const router = express.Router()
const db = require('../../db')
const $ = require('../../api-v1/$');
const auth = require('../auth.mware');

router.route('/')
  .get(async (req, res) => {
    const places = await $._find_All('Places', {})
    res.send({ places })
  })

router.route('/')
  .post(auth, async (req, res) => {
    const {
      name,
      images,
      description,
      onMain
    } = req.body

    var newArticle = new db.Places();
    newArticle.name = name
    newArticle.images = images || []
    newArticle.description = description
    newArticle.onMain = onMain

    var saved = await $._saveInBase(newArticle)

    res.send({ place: saved })
  })


router.route('/:id')
  .put(auth, async (req, res) => {
    const id = req.params.id

    const {
      name,
      images,
      description,
      type,
      onMain
    } = req.body

    newArticle = await $._find_One('Places', { _id: id })

    newArticle.name = name
    newArticle.images = images
    newArticle.description = description
    newArticle.type = type
    newArticle.onMain = onMain
    var saved = await $._saveInBase(newArticle)

    res.send({ place: saved })
  })

router.route('/:id')
  .delete(auth, async (req, res) => {
    const id = req.params.id
    await $._delete('Places', { _id: id })
    res.send({ status: true })
  })

module.exports = router;
