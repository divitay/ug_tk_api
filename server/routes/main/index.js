const checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$")
const express = require('express')
const router = express.Router()
const db = require('../../db')
const nodemailer = require('nodemailer')
const $ = require('../../api-v1/$');

//	опции node-mailer
const transporter = nodemailer.createTransport({
  host: 'smtp.yandex.ru',
  auth: {
    user: 'no_reply@poedimdoma.ru',
    pass: '741852963'
  }
});

router.route('/feedback')
  .post((req, res) => {
    let body = req.body;
    if (body.description && body.phone && body.fio && body.position) {
      var htmlToSend = '<br /> ФИО : ' + body.fio + ' <br /> Должность : ' + body.position + '  <br /> Телефон : ' + body.phone + ' <br /> О себе : ' + body.description;
      var mailOptions = {
        from: ' <no_reply@poedimdoma.ru>',
        subject: 'Отклик на вакансию',
        text: 'Отклик на вакансию',
        to: 'oksana_muhortova@mail.ru',
        html: htmlToSend
      };
      transporter.sendMail(mailOptions, function (err) {
        if (err) {
          console.log(err)
          return res.send(err);
        }
        // Сохраняем нового пользователя
        return res.send({ success: 'Ваше обращение будет рассмотрено в ближайшее время.' });
      });
    } else if (body.description && body.phone && body.fio && body.review) {
      var htmlToSend = '<br /> ФИО : ' + body.fio + '  <br /> Телефон : ' + body.phone + ' <br /> Отзыв : ' + body.description;
      var mailOptions = {
        from: ' <no_reply@poedimdoma.ru>',
        subject: 'Отзыв',
        text: 'Отзыв пользователя с сайта',
        to: 'hizovaleksandr@gmail.com, sushi-pizza.office@mail.ru, art-sushi@bk.ru',
        html: htmlToSend
      };
      transporter.sendMail(mailOptions, function (err) {
        if (err) {
          console.log(err)
          return res.send(err);
        }
        // Сохраняем нового пользователя
        return res.send({ success: 'Ваше обращение будет рассмотрено в ближайшее время.' });
      });
    } else {
      return res.send({ error: "Не все поля заполнены" });
    }
  });

router.route('/Comments')
  .get(async (req, res) => {
    const stopList = await redis.get('menu:stoplist')
    res.send(stopList)
  })

router.route('/category/:url')
  .get((req, res, next) => {
    let url = (req.params.url + '').toLowerCase();
    let errUrl;
    // Для старой версии соответствие урлов
    if (!isNaN(url)) {
      switch (url) {
        case '15': url = 'rolls'; break; case '19': url = 'baked_rolls'; break; case '20': url = 'hot_rolls'; break;
        case '18': url = 'sets'; break; case '7': url = 'pizza'; break; case '24': url = 'on_the_grill'; break;
        case '23': url = 'georgian_cuisine'; break; case '14': url = 'snacks'; break; case '11': url = 'soups'; break;
        case '13': url = 'burgers_and_sandwiches'; break; case '10': url = 'pasta'; break; case '12': url = 'salads'; break;
        case '8': url = 'focaccia_pies_bread'; break; case '9': url = 'hot_meals'; break; case '6': url = 'garnish_sauce'; break;
        case '5': url = 'desserts'; break; case '21': url = 'drinks'; break;
      }
    }
    console.log(url)
    tplData.template.view = 'main_page';
    tplData.template.layout = 'mainVue';
    tplData.template.component = 'category-page';
    // async
    for (var i = 0; i < tplData.list.groups.length; i++) {
      if (tplData.list.groups[i] && tplData.list.groups[i].url == url) {
        tplData.main = tplData.list.groups[i];
      }
    }
    if (!tplData.main.id) {
      errUrl = true;
    } else {
      tplData.main.items = [];
      for (var j = 0; j < tplData.list.products.length; j++) {
        if (tplData.main && tplData.main.id && (tplData.main.id == tplData.list.products[j].parentGroup)) {
          tplData.main.items.push(tplData.list.products[j]);
        }
      }
    }
    if (errUrl) {
      return next();
    }
    if (req.query && req.query.json == '') {
      return res.send(tplData);
    } else {
      // res.send({'tplData': tplData}); 'isMobile' || 'isTablet'
      if ((tplData.isMobile == true) || (tplData.isTablet == true)) {
        return res.render('index', tplData);
      } else {
        return render(res, 'index', tplData);
      }
    }
  });

router.route('/item/:id')
  .get((req, res, next) => {
    let id = (req.params.id + '').toLowerCase();
    // Для старой версии соответствие урлов
    tplData.template.view = 'main_page';
    tplData.template.layout = 'mainVue';
    tplData.template.component = 'item-page';
    // async
    if (!checkForHexRegExp.test(id)) return next();
    $._find_ById('Action', id)
      .then((data) => {
        if (data) {
          tplData.main = data;
          if (req.query && req.query.json == '') {
            return res.send(tplData);
          } else if ((tplData.isMobile == true) || (tplData.isTablet == true)) {
            return res.render('index', tplData);
          } else {
            return render(res, 'index', tplData);
          }
        } else {
          next();
        }

      })
      .catch((err) => {
        if (err) {
          console.log('error in item/' + id, err)
          next();
        }
      });
  });
// /////////////
// ASYNC
// /////////////
let getOtherPages = function (tplData, url, callback) {
  let getOtherPageAndItems = async((url) => {
    let obj = {};
    try {
      let section = await($._find_One('Action', { 'urlId': url, 'typeTree': 'Action.Section' }));

      if (section && section._id) {
        tplData.main = section;
        let query
        if (url === 'actions') {
          query = { 'parentId': section._id, 'dates.created': { $gte: new Date("2019-11-25T23:59:59Z").toISOString() } }
        } else {
          query = { 'parentId': section._id }
        }
        let arr = await($._find_All('Action', query));
        arr.sort((a, b) => {
          let one = (new Date(a.dates.created)).getTime();
          let two = (new Date(b.dates.created)).getTime();
          if (one > two) {
            return -1;
          }
          if (one < two) {
            return 1;
          }
          return 0
        });
        tplData.list.items = arr;
      } else {
        return obj.errUrl = true;
      }
    } catch (err) {
      console.log('error as/aw', err);
    }
    return obj;
  });
  getOtherPageAndItems(url)
    .then((data) => {
      if (data.errUrl) {
        return callback(null, null);
      }
      return callback(null, tplData);

    })
    .catch((err) => {
      if (err) {
        return callback(err, tplData);
      }
    });
}
// /////////////
// /////////////
// /////////////
router.route('/:url')
  .get((req, res, next) => {
    let url = (req.params.url + '').toLowerCase();
    // console.log('???lololo');
    if ((url != 'actions') && (url != 'news')) return next();
    // console.log('lololo');
    // let tplData = _tplData(req);
    tplData.template.view = 'main_page';
    tplData.template.layout = 'mainVue';
    tplData.template.component = 'simple-page';
    getOtherPages(tplData, url, function (err, result) {
      if (err) {
        console.log(err);
      }
      if (!result) next();
      if (req.query && req.query.json == '') {
        return res.send(tplData);
      } else if ((tplData.isMobile == true) || (tplData.isTablet == true)) {
        return res.render('index', tplData);
      } else {
        return render(res, 'index', tplData);
      }
    });
  });

router.route('/pages/:url')
  .get((req, res, next) => {
    let url = (req.params.url + '').toLowerCase();
    // let tplData = _tplData(req);
    tplData.template.view = 'main_page';
    tplData.template.layout = 'mainVue';
    tplData.template.component = 'simple-page';
    getOtherPages(tplData, url, function (err, result) {
      if (err) {
        console.log(err);
      }
      if (!result) next();
      if (req.query && req.query.json == '') {
        return res.send(tplData);
      } else if ((tplData.isMobile == true) || (tplData.isTablet == true)) {
        return res.render('index', tplData);
      } else {
        return render(res, 'index', tplData);
      }
    });
  });

module.exports = router;

// // Сумма должна быть указана в копейках. Тоесть вместо 100.00 должно быть 10000, для этого умнажаем на 100.
// let price = (+(summ) * 100);
// // После того как пройдет оплата нам нужно на наш обработчик кинуть callback url с orderId который успешно оплачен
// let orderId = orderId; // orderId
// //callback url c orderId, можно накидать сюда все что хотим. Переход будет выполнен на этот url после успешной оплаты
// // поэтому нам нужно передать в query необходимые для нашего обработчика параметры
// let returnUrl = 'http://localhost:5000/\?payment=success&orderId='+orderId; 
// // Финальный url выглядит примерно так
// let	sbUrl = 'https://securepayments.sberbank.ru/payment/rest/register.do?amount=' + String(price) + '&language=ru&orderNumber=' + orderId + '&password=' + password + '&returnUrl=' + returnUrl + '&userName=' + login;