const config = require("../cfg")

module.exports = (req, res, next) => {
    if (config.auth) {
        if (req.headers.autorization !== config.authToken) {
            return res.status(403).send()
        }
    }
    return next()
}