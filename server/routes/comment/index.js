const express = require('express')
const router = express.Router()
const db = require('../../db')
const $ = require('../../api-v1/$');
const auth = require('../auth.mware')

router.route('/not-approved')
  .get(async (req, res) => {
    const comments = await $._find_All('Comments', { approved: false })
    res.send({ comments })
  })

router.route('/')
  .post(async (req, res) => {
    const {
      roomId,
      name,
      text,
      rating
    } = req.body

    var newComment = new db.Comments();
    newComment.roomId = roomId
    newComment.name = name
    newComment.text = text
    newComment.rating = rating

    var savedComment = await $._saveInBase(newComment)

    res.send({ comment: savedComment })
  })

router.route('/approve/:id')
  .put(auth, async (req, res) => {
    const id = req.params.id

    const comment = await $._find_One('Comments', { _id: id })
    comment.approved = true

    var savedComment = await $._saveInBase(comment)
    res.send({ comment: savedComment })
  })

router.route('/:id')
  .delete(auth, async (req, res) => {
    const id = req.params.id
    await $._delete('Comments', { _id: id })
    res.send({ status: true })
  })

router.route('/:roomId')
  .get(async (req, res) => {
    const roomId = req.params.roomId
    const comments = await $._find_All('Comments', { roomId })
    res.send({ comments })
  })
module.exports = router;
