const express = require('express')
const router = express.Router()
const db = require('../../db')
const $ = require('../../api-v1/$');
const auth = require('../auth.mware');

router.route('/')
  .get(async (req, res) => {
    const articles = await $._find_All('Articles', {})
    res.send({ articles })
  })

router.route('/:type')
  .get(async (req, res) => {
    const type = req.params.type
    const articles = await $._find_All('Articles', { type })
    res.send({ articles })
  })

router.route('/')
  .post(auth, async (req, res) => {
    const {
      name,
      images,
      description,
      type,
      onMain
    } = req.body

    var newArticle = new db.Articles();
    newArticle.name = name
    newArticle.images = images
    newArticle.description = description
    newArticle.type = type
    newArticle.onMain = onMain
    var saved = await $._saveInBase(newArticle)

    res.send({ article: saved })
  })

router.route('/:id')
  .put(auth, async (req, res) => {
    const id = req.params.id

    const {
      name,
      images,
      description,
      type,
      onMain
    } = req.body

    newArticle = await $._find_One('Articles', { _id: id })

    newArticle.name = name
    newArticle.images = images
    newArticle.description = description
    newArticle.type = type
    newArticle.onMain = onMain
    var saved = await $._saveInBase(newArticle)

    res.send({ article: saved })
  })

router.route('/:id')
  .delete(auth, async (req, res) => {
    const id = req.params.id
    await $._delete('Articles', { _id: id })
    res.send({ status: true })
  })

module.exports = router;
