const express = require('express')
const router = express.Router()
const db = require('../../db')
const $ = require('../../api-v1/$');
const auth = require('../auth.mware');

router.route('/')
  .get(async (req, res) => {
    const roomId = req.params.roomId
    const rooms = await $._find_All('Rooms', { roomId })
    res.send({ rooms })
  })

router.route('/')
  .post(auth, async (req, res) => {
    const {
      name,
      categoryId,
      tags,
      address,
      lat,
      lng,
      images,
      price,
      priceJune,
      priceJuly,
      priceAugust,
      description,
      onMain
    } = req.body

    if (!categoryId) {
      res.status(400).send({ error: 'categoryId required'})
    }
    var newRoom = new db.Rooms();
    newRoom.name = name
    newRoom.address = address
    newRoom.categoryId = categoryId
    newRoom.tags = tags || []
    newRoom.lat = lat
    newRoom.lng = lng
    newRoom.images = images || []
    newRoom.price = price
    newRoom.description = description
    newRoom.onMain = onMain
    newRoom.priceJune = priceJune
    newRoom.priceJuly = priceJuly
    newRoom.priceAugust = priceAugust

    var savedRoom = await $._saveInBase(newRoom)

    res.send({ room: savedRoom })
  })

router.route('/:id')
  .put(auth, async (req, res) => {
    const id = req.params.id
    const {
      name,
      categoryId,
      tags,
      address,
      lat,
      lng,
      images,
      price,
      priceJune,
      priceJuly,
      priceAugust,
      description,
      onMain
    } = req.body

    if (!categoryId) {
      res.status(400).send({ error: 'categoryId required'})
    }

    const newRoom = await $._find_One('Rooms', { _id: id })
    newRoom.name = name
    newRoom.address = address
    newRoom.categoryId = categoryId
    newRoom.tags = tags || []
    newRoom.lat = lat
    newRoom.lng = lng
    newRoom.images = images || []
    newRoom.price = price
    newRoom.priceJune = priceJune
    newRoom.priceJuly = priceJuly
    newRoom.priceAugust = priceAugust
    newRoom.description = description
    newRoom.onMain = onMain

    var savedRoom = await $._saveInBase(newRoom)

    res.send({ room: savedRoom })
  })

router.route('/:id')
  .delete(auth, async (req, res) => {
    const id = req.params.id
    await $._delete('Rooms', { _id: id })
    res.send({ status: true })
  })

module.exports = router;
