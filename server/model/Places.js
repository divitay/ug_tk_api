// Standard mongoose requires
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

var placesSchema = new Schema({
	name: { type: String, index: false },
	type: { type: String, index: false },
	images: { type: [String], index: true },
	description: { type: String, index: false },
	onMain: { type: Boolean, default: false, index: true },
	createdAt : { type: Date, default: new Date(), index: true }
}, { strict: false });

module.exports = mongoose.model('Places', placesSchema);