// Standard mongoose requires
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

var articlesSchema = new Schema({
	roomId: { type: ObjectId, index: true },
	name: { type: String, index: true },
	text: { type: String, index: true },
	rating: { type: Number, index: true },
	approved: { type: Boolean, default: false, index: true },
	createdAt : { type: Date, default: new Date(), index: true }
}, { strict: false });

module.exports = mongoose.model('Comments', articlesSchema);