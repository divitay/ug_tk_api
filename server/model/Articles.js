// Standard mongoose requires
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// var ObjectId = Schema.Types.ObjectId;

var roomsSchema = new Schema({
	name: { type: String, index: false },
	type: { type: String, index: false },
	images: { type: [String], index: true },
	type: { type: String, default: 'story', index: false },
	description: { type: String, index: false },
	onMain: { type: Boolean, default: false, index: true },
	createdAt : { type: Date, default: new Date(), index: true }
}, { strict: false });

module.exports = mongoose.model('Articles', roomsSchema);