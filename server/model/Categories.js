// Standard mongoose requires
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// var ObjectId = Schema.Types.ObjectId;

var categorySchema = new Schema({
	name: { type: String, index: false },
	description: { type: String, index: false },
	createdAt : { type: Date, default: new Date(), index: true }
}, { strict: false });

module.exports = mongoose.model('Categories', categorySchema);