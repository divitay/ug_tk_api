// Standard mongoose requires
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

var roomsSchema = new Schema({
	name: { type: String, index: false },
	address: { type: String, index: false },
	lat: { type: Number, index: true },
	lng: { type: Number, index: true },
	images: { type: [String], index: true },
	price: { type: Number, index: true },
	priceJune: { type: Number, index: true },
	priceJuly: { type: Number, index: true },
	priceAugust: { type: Number, index: true },
	description: { type: String, index: false },
	categoryId: { type: ObjectId, index: true },
	tags: { type: [String], index: true },
	onMain: { type: Boolean, default: false, index: true },
	createdAt : { type: Date, default: new Date(), index: true }
}, { strict: false });

module.exports = mongoose.model('Rooms', roomsSchema);